use gtk::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib};

use crate::widgets::AccountAddDialog;

mod imp {
    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate)]
    #[template(file = "data/resources/ui/window.blp")]
    pub struct MailWindow {
        // Template widgets
        #[template_child]
        pub header_bar: TemplateChild<adw::HeaderBar>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MailWindow {
        const NAME: &'static str = "MailWindow";
        type Type = super::MailWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();

            klass.install_action("win.add_account", None, |win, _, _| {
            	win.open_add_account();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for MailWindow {
    	fn constructed(&self) {
				self.parent_constructed();
	  	}
    }
    impl WidgetImpl for MailWindow {}
    impl WindowImpl for MailWindow {}
    impl ApplicationWindowImpl for MailWindow {}
    impl AdwApplicationWindowImpl for MailWindow {}
}

glib::wrapper! {
    pub struct MailWindow(ObjectSubclass<imp::MailWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,        @implements gio::ActionGroup, gio::ActionMap;
}

impl MailWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        glib::Object::builder()
            .property("application", application)
            .build()
    }

    pub fn open_add_account(&self) {
    	let dialog = AccountAddDialog::new();
    	dialog.set_transient_for(Some(self));
    	dialog.present();
    }
}
