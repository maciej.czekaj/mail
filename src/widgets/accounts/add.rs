use adw::{prelude::*, subclass::prelude::*};
use gtk::glib;
use log;
use regex::Regex;

mod imp {
	use std::cell::{RefCell};
	use gtk::CompositeTemplate;
	use super::*;

	#[derive(CompositeTemplate, Debug, Default)]
	#[template(file = "data/resources/ui/account_add.blp")]
	pub struct AccountAddDialog {
		pub email: RefCell<String>,

		#[template_child]
		pub next: TemplateChild<gtk::Button>,
		#[template_child]
		pub navigation_view: TemplateChild<adw::NavigationView>,
	}

	#[glib::object_subclass]
	impl ObjectSubclass for AccountAddDialog {
		const NAME: &'static str = "AccountAddDialog";
		type Type = super::AccountAddDialog;
		type ParentType = adw::Window;

		fn class_init(klass: &mut Self::Class) {
			klass.bind_template();
			klass.bind_template_instance_callbacks();

			klass.install_action("add.previous", None, |dialog, _, _| {
				let imp = dialog.imp();
				if imp
					.navigation_view
					.visible_page()
					.and_then(|page| page.tag())
					.is_some_and(|tag| tag != "main") {
						imp.navigation_view.pop();
					} else {
						dialog.close();
					}
			});

			klass.install_action("add.next", None, |dialog, _, _| {
				let imp = dialog.imp();
				imp.obj().switch_view("manual_config");
			});
		}

		fn instance_init(obj: &gtk::glib::subclass::InitializingObject<Self>) {
			obj.init_template();
		}
	}

	impl ObjectImpl for AccountAddDialog {
		fn constructed(&self) {
			self.parent_constructed();
			self.obj().action_set_enabled("add.next", false);
		}
	}
	impl WidgetImpl for AccountAddDialog {}
	impl WindowImpl for AccountAddDialog {}
	impl AdwWindowImpl for AccountAddDialog {}
}

glib::wrapper! {
	pub struct AccountAddDialog(ObjectSubclass<imp::AccountAddDialog>)
		@extends gtk::Widget, gtk::Window, adw::Window;
}

#[gtk::template_callbacks]
impl AccountAddDialog {
	pub fn new() -> Self {
		glib::Object::builder().build()
	}

	#[template_callback]
	fn on_email_changed(&self, entry: Option<gtk::Editable>) {
		let imp = self.imp();
		let binding = entry.expect("Editable component missing").text();
		let email = binding.as_str();
		let is_email_regex = Regex::new(r"^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,}$")
			.expect("Unable to create regex");

		let is_email_valid = is_email_regex.is_match(email);
		log::info!("Email valid: {}", is_email_valid);

		imp.obj().action_set_enabled("add.next", is_email_valid);
	}

	#[template_callback]
	pub fn switch_view(&self, tag: &str) {
		log::info!("Tag: {}", tag);
		let imp = self.imp();
		imp.navigation_view.push_by_tag(tag);
	}
}
